<?php
namespace App\Controller;

use App\Entity\Pharmacie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PharmacieRepository;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class PharmacieController extends AbstractController
{
    /**
     * 
     */
    public function index(PharmacieRepository $pharmacieRepository, SerializerInterface $serializer): JsonResponse
    {
        $pharmacies = $pharmacieRepository->findAll();

        // on convertit le resultat en json
        $data = $serializer->serialize($pharmacies, 'json');

        // on renvoie la liste des pharmacies
        return new JsonResponse($data, 200, [], true);
    }

    public function post(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $data = $request->toArray();

        $pharmacie = new Pharmacie();

        $pharmacie->setNom($data['nom']);
        $pharmacie->setQuartier($data['quartier']);
        $pharmacie->setVille($data['ville']);
        $pharmacie->setGarde($data['garde']);

        $entityManager->persist($pharmacie);

        $entityManager->flush();

        return new Response('Saved new product with id '.$pharmacie->getId());
    }

    public function delete($id, PharmacieRepository $pharmacieRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $pharmacie = $pharmacieRepository->find($id);

        if ($pharmacie != null)
        {
            $entityManager->remove($pharmacie);

            $entityManager->flush();
    
            return new Response('Deleted pharmacie with id '.$id);
        }
        
        return new Response('error');
    }

    public function put($id, Request $request, PharmacieRepository $pharmacieRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $pharmacie = $pharmacieRepository->find($id);

        $data = $request->toArray();

        if($pharmacie != null)
        {
            $pharmacie->setNom($data['nom']);
            $pharmacie->setQuartier($data['quartier']);
            $pharmacie->setVille($data['ville']);
            $pharmacie->setGarde($data['garde']);
    
            $entityManager->persist($pharmacie);
    
            $entityManager->flush();

            return new Response('Updated pharmacie with id '.$pharmacie->getId());
        }

        return new Response('No pharmacie');
    }

    public function garde(PharmacieRepository $pharmacieRepository, SerializerInterface $serializer): JsonResponse
    {
        $jour = date('w');

        switch ($jour) {
            case 0:
                $jour = 'dimanche';
                break;
            case 1:
                $jour = 'lundi';
                break;
            case 2:
                $jour = 'mardi';
                break;
            case 3:
                $jour = 'mercredi';
                break;
            case 4:
                $jour = 'jeudi';
                break;
            case 5:
                $jour = 'vendredi';
                break;
            case 6:
                $jour = 'samedi';
                break;
            }

        
        $pharmacies = $pharmacieRepository->findByGarde($jour);

        // on convertit le resultat en json
        $data = $serializer->serialize($pharmacies, 'json');

        // on renvoie la liste des pharmacies
        return new JsonResponse($data, 200, [], true);
    }
}
